/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Małgorzata
 */
public class Point3i {
    
    public int x;
    public int y;
    public int z;

    public Point3i()
    {
        x = 0;
        y = 0;
    }

    public Point3i(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
    

