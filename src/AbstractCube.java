import java.awt.Point;


/**
 *
 * @author Małgorzata
 * 
 * Klasa odpowiadająca za kostkę abstrakcyjną.
 * 
 * Jest to trójwymiarowa tablica z zapisanymi numerami kosteczek 
 * tworzącymi rzeczywistą kostkę Rubika.
 */

public class AbstractCube {
    
    int[][][] kostka;
    static Point iteratorFrom = new Point();
    static Point iteratorTo = new Point();
    static Point From = new Point();
    static Point To = new Point();
    static Point3i start = new Point3i();
    static Point3i stop = new Point3i();

    public AbstractCube(int[][][] kostka) {
        this.kostka = kostka;
    }
        
    
    // Funkcja odpowiadająca za obrót kostki abstrakcyjnej
    public void Rotation(String akcja){
        
        // Wybranie odpowiednich wartości początkowych 
        // dla odpowiedniego obrotu warstwy
        if ("R".equals(akcja))
        {
            start = new Point3i(2, 0, 0);
            stop = new Point3i(2, 0, 2);

            From.x = start.y;
            From.y = start.z;

            To.x = stop.y;
            To.y = stop.z;
        }

        else if ("R'".equals(akcja))
        {
            start = new Point3i(2, 0, 0);
            stop = new Point3i(2, 0, 2);

            From.x = stop.y;
            From.y = stop.z;

            To.x = start.y;
            To.y = start.z;
        }
        
        else if ("L".equals(akcja))
        {
            start = new Point3i(0, 0, 0);
            stop = new Point3i(0, 2, 0);

            From.x = start.y;
            From.y = start.z;

            To.x = stop.y;
            To.y = stop.z;
        }
        
        else if ("L'".equals(akcja))
        {
            start = new Point3i(0, 0, 0);
            stop = new Point3i(0, 2, 0);

            From.x = stop.y;
            From.y = stop.z;

            To.x = start.y;
            To.y = start.z;
        }
        
        else if ("U".equals(akcja))
        {
            start = new Point3i(0, 0, 2);
            stop = new Point3i(0, 2, 2);

            From.x = start.x;
            From.y = start.y;

            To.x = stop.x;
            To.y = stop.y;
        }
        
        else if ("U'".equals(akcja))
        {
            start = new Point3i(0, 0, 2);
            stop = new Point3i(0, 2, 2);

            From.x = stop.x;
            From.y = stop.y;

            To.x = start.x;
            To.y = start.y;
        }
        
        else if ("D".equals(akcja))
        {
            start = new Point3i(0, 0, 0);
            stop = new Point3i(2, 0, 0);

            From.x = start.x;
            From.y = start.y;

            To.x = stop.x;
            To.y = stop.y;
        }
        
        else if ("D'".equals(akcja))
        {
            start = new Point3i(0, 0, 0);
            stop = new Point3i(2, 0, 0);

            From.x = stop.x;
            From.y = stop.y;

            To.x = start.x;
            To.y = start.y;
        }
        
        else if ("F".equals(akcja))
        {
            start = new Point3i(0, 0, 0);
            stop = new Point3i(0, 0, 2);

            From.x = start.x;
            From.y = start.z;

            To.x = stop.x;
            To.y = stop.z;
        }
        
        else if ("F'".equals(akcja))
        {
            start = new Point3i(0, 0, 0);
            stop = new Point3i(0, 0, 2);

            From.x = stop.x;
            From.y = stop.z;

            To.x = start.x;
            To.y = start.z;
        }
        
        else if ("B".equals(akcja))
        {
            start = new Point3i(0, 2, 0);
            stop = new Point3i(2, 2, 0);

            From.x = start.x;
            From.y = start.z;

            To.x = stop.x;
            To.y = stop.z;
        }
        
        else if ("B'".equals(akcja))
        {
            start = new Point3i(0, 2, 0);
            stop = new Point3i(2, 2, 0);

            From.x = stop.x;
            From.y = stop.z;

            To.x = start.x;
            To.y = start.z;
        }


        int[][] tempWarstwa = new int[3][3];

        // Poszczególne wartości w wybranej warstie są przemieszczane zgodnie 
        // z przemmieszaniem się kostek w realnej kostce rubika
        for (int i = 0; i < 8; i++)
        {
            // Przemieszczenie kostek w zależności od akcji
            if ("R".equals(akcja) | "R'".equals(akcja) | "L".equals(akcja) | 
                    "L'".equals(akcja)) tempWarstwa[To.x][To.y] = kostka[start.x][From.x][From.y];
            if ("U".equals(akcja) | "U'".equals(akcja) | "D".equals(akcja) | 
                    "D'".equals(akcja)) tempWarstwa[To.x][To.y] = kostka[From.x][From.y][start.z];
            if ("F".equals(akcja) | "F'".equals(akcja) | "B".equals(akcja) | 
                    "B'".equals(akcja)) tempWarstwa[To.x][To.y] = kostka[From.x][start.y][From.y];

            if (From.x == 0 & From.y == 0)
            {
                iteratorFrom.x = 0;
                iteratorFrom.y = 1;
            }
            if (From.x == 0 & From.y == 2)
            {
                iteratorFrom.x = 1;
                iteratorFrom.y = 0;
            }
            if (From.x == 2 & From.y == 2)
            {
                iteratorFrom.x = 0;
                iteratorFrom.y = -1;
            } 
            if (From.x == 2 & From.y == 0)
            {
                iteratorFrom.x = -1;
                iteratorFrom.y = 0;
            }

            From.x += iteratorFrom.x;
            From.y += iteratorFrom.y;

            if (To.x == 0 & To.y == 0)
            {
                iteratorTo.x = 0;
                iteratorTo.y = 1;
            }
            if (To.x == 0 & To.y == 2)
            {
                iteratorTo.x = 1;
                iteratorTo.y = 0;
            }
            if (To.x == 2 & To.y == 2)
            {
                iteratorTo.x = 0;
                iteratorTo.y = -1;
            }
            if (To.x == 2 & To.y == 0)
            {
                iteratorTo.x = -1;
                iteratorTo.y = 0;
            }

            To.x += iteratorTo.x;
            To.y += iteratorTo.y;
        }
        
        // Zastąpienie poprzedniej warstwy warstwą obróconą
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (i == 1 & j == 1) continue;
                if ("R".equals(akcja) | "R'".equals(akcja) | "L".equals(akcja) | 
                        "L'".equals(akcja)) kostka[start.x][i][j] = tempWarstwa[i][j];
                if ("U".equals(akcja) | "U'".equals(akcja) | "D".equals(akcja) | 
                        "D'".equals(akcja)) kostka[i][j][start.z] = tempWarstwa[i][j];
                if ("F".equals(akcja) | "F'".equals(akcja) | "B".equals(akcja) | 
                        "B'".equals(akcja)) kostka[i][start.y][j] = tempWarstwa[i][j];
            }
        }
    }
    
    
    // Funkcja zwraca położenie kosteczki
    // (z rzeczywistej kostki) w kostce abstrakcyjnej
    public Point3i getPosition(int nr){
       int x = 0;
       int y = 0;
       int z = 0;
       
       for (; x < 3; x++) {
           for (; y < 3; y++) {
               for (; z < 3; z++) {
                   if(kostka[x][y][z] == nr) break;
               }
               if(z == 3) z = 0;
                if(kostka[x][y][z] == nr) break;   
           }
           if(y == 3)y = 0;
           if(kostka[x][y][z] == nr) break;
       }
       
       return new Point3i(x,y,z);
   }
    
}
