import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.pickfast.PickIntersection;
import com.sun.j3d.utils.pickfast.PickTool;
import java.awt.event.MouseEvent;
import javax.media.j3d.PickInfo;
import javax.media.j3d.Transform3D;
import javax.vecmath.Point3d;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.Shape3D;
import static java.lang.Math.abs;

/**
 *
 * @author Kamil
 * 
 * Klasa obsługująca obroty warstw za pomocą myszki;
 * 
 */

public class MouseManual {
    
    private PickInfo[] pickInfoArr;
    private RubikCube rubikcube;
    private Point3d pierwszaPozycjaNaKostce;
    private Point3d pierwszaPozycjaWPrzestrzeni;
    private Point3i pozycjaKosteczki;
    private RotationData ObracanaWarstwa;
    private boolean WarstwaWybrana;
    private double kacik = 0;
    
    
    public MouseManual(RubikCube rubikcube){
        this.ObracanaWarstwa = new RotationData("", '/', 0, 0);
        this.rubikcube = rubikcube;
        this.pierwszaPozycjaNaKostce = new Point3d();
        this.pierwszaPozycjaWPrzestrzeni = new Point3d();
        this.WarstwaWybrana = false;
    }
    
    
    public Point3d pozycjaMyszkiNaKostce(MouseEvent e){
        rubikcube.pickCanvas.setShapeLocation(e);
        
        // Pobieramy informacje o elementach, na które wskazuje myszka
        pickInfoArr = rubikcube.pickCanvas.pickAllSorted();
        
        if (pickInfoArr != null) {
            // Wybieramy najbliższy element
            Transform3D l2vw = pickInfoArr[0].getLocalToVWorld();
            PickInfo.IntersectionInfo[] iInfoArr = pickInfoArr[0].getIntersectionInfos();
            PickIntersection pi = new PickIntersection(l2vw, iInfoArr[0]);

            return pi.getPointCoordinatesVW();
        }
        
        return null;
    }
    
    
    public Point3d pozycjaMyszkiWprzestrzeni(MouseEvent e){
        Point3d temp = new Point3d();
        rubikcube.canvas3D.getPixelLocationInImagePlate(e.getX(), e.getY(), temp);
        Transform3D transform = new Transform3D();
        rubikcube.canvas3D.getImagePlateToVworld(transform);
        transform.transform(temp); 
        return temp;
    }
    
    
    public int kliknietaKosteczka(MouseEvent e){
        rubikcube.pickCanvas.setShapeLocation(e);
        
        // Pobieramy informacje o elemnentach, na które wskazuje myszka
        pickInfoArr = rubikcube.pickCanvas.pickAllSorted();
        PickTool pickTool = new PickTool(rubikcube.scena);
        Shape3D s = null;
        
        try
        {
            // Wyciągnięcie właściwego elementu (ściany kosteczki) z wcześniej uzyskanych informacji
           s = (Shape3D)pickTool.getNode(pickInfoArr[0], PickTool.TYPE_SHAPE3D);
        }
        catch(NullPointerException ex){ 
            
        }
        if (pickInfoArr != null){
            // Jeśli odczytaliśmy odpowiedni element, zwracamy jego numer
             return (int)s.getUserData();
        }
        
        return -1;
    }
    
    
    public void mousePressed(MouseEvent e)
    {
        pierwszaPozycjaNaKostce = pozycjaMyszkiNaKostce(e);
        pierwszaPozycjaWPrzestrzeni = pozycjaMyszkiWprzestrzeni(e);
            
        
        if(pierwszaPozycjaNaKostce != null){
            
            // Pobranie pozycjcji kosteczki z tablicy trójwymiarowej (z kostki abstrakcyjnej)
            pozycjaKosteczki = rubikcube.abstractCube.getPosition(kliknietaKosteczka(e));
            
            // Wyłączenie możliwość obrotu widoku
            rubikcube.simpleU.getViewingPlatform().setViewPlatformBehavior(null);
        }
        
    }
    
    
    public void mouseDragged(MouseEvent e)
    {
        Point3d ObecnaPozycja = pozycjaMyszkiNaKostce(e);
        if(!WarstwaWybrana)
        {
            if(ObecnaPozycja == null | pierwszaPozycjaNaKostce == null) return;
            
            // Obliczenie wektora przemieszczenia myszki
            Point3d wektor = new Point3d();
            wektor.x = ObecnaPozycja.x - pierwszaPozycjaNaKostce.x;
            wektor.y = ObecnaPozycja.y - pierwszaPozycjaNaKostce.y;
            wektor.z = ObecnaPozycja.z - pierwszaPozycjaNaKostce.z;
            
            // Obliczenie wartości bezwzględnych współrzędnych wektora
            Point3d temp = new Point3d();
            temp.x = abs(wektor.x);
            temp.y = abs(wektor.y);
            temp.z = abs(wektor.z);

            if (temp.x > 0.03 | temp.y > 0.03 | temp.z > 0.03)
            {
                char osObrotu = '/';
                
                // Szukamy współrzędnej, której wartość bezwzględna jest pomiędzy pozostałymi dwiema
                if ((temp.y > temp.z && temp.y < temp.x) || (temp.y < temp.z && temp.y > temp.x))
                    osObrotu = 'y';
                else if ((temp.z > temp.x && temp.z < temp.y) || (temp.z < temp.x && temp.z > temp.y))
                    osObrotu = 'z';
                else if ((temp.x > temp.z && temp.z < temp.y) || (temp.x < temp.z && temp.z > temp.y))
                    osObrotu = 'x';

                int nrWarstwy = 0;
                
                // Wybór warstww w kostce abstrakcyjnej na podstawie wybranej kosteczki
                if(osObrotu == 'x') nrWarstwy = pozycjaKosteczki.x;
                else if(osObrotu == 'y') nrWarstwy = pozycjaKosteczki.y;
                else if(osObrotu == 'z') nrWarstwy = pozycjaKosteczki.z;
                
                // Szukanie obrotu, który odpowiada uzyskanym danym
                for (RotationData obr : rubikcube.cubeRotation.daneObrotow) {
                   if(obr.os == osObrotu & obr.warstwa== nrWarstwy){
                        
                        WarstwaWybrana = true;
                        ObracanaWarstwa = obr;
                        break;
                   }
                }
            }
        }
        else {   
            // Po wyborze obracanej warstwy wyliczamy kąt pomiędzy wektorem pozycji 
            // początkowej a wektorem aktualnej pozycji myszki
            // Zakładamy, że oba wektory są zaczepione w początku układu współrzędnych
            ObecnaPozycja = pozycjaMyszkiWprzestrzeni(e);
            
            double licznik = 0;
            double dl1 = 0;
            double dl2 = 0;
            
            // W zależności od rodzaju obrotu (osi obrotu) rzutujemy wektory na odpowiednią płaszczyznę obrotu
            // Obliczamy sinus wspomnianego wcześniej kąta pomiędzy wektorami
            if("R".equals(ObracanaWarstwa.obrot) | "L".equals(ObracanaWarstwa.obrot))
            {
                licznik = ObecnaPozycja.y * pierwszaPozycjaWPrzestrzeni.z - ObecnaPozycja.z * pierwszaPozycjaWPrzestrzeni.y;
                dl1 = Math.hypot(ObecnaPozycja.y, ObecnaPozycja.z);
                dl2 = Math.hypot(pierwszaPozycjaWPrzestrzeni.y, pierwszaPozycjaWPrzestrzeni.z);
            }
            else if("U".equals(ObracanaWarstwa.obrot) | "D".equals(ObracanaWarstwa.obrot))
            {
                licznik = ObecnaPozycja.x * pierwszaPozycjaWPrzestrzeni.y - ObecnaPozycja.y * pierwszaPozycjaWPrzestrzeni.x;
                dl1 = Math.hypot(ObecnaPozycja.x, ObecnaPozycja.y);
                dl2 = Math.hypot(pierwszaPozycjaWPrzestrzeni.x, pierwszaPozycjaWPrzestrzeni.y);
            }
            else if("F".equals(ObracanaWarstwa.obrot) | "B".equals(ObracanaWarstwa.obrot))
            {
               licznik = ObecnaPozycja.z * pierwszaPozycjaWPrzestrzeni.x - ObecnaPozycja.x * pierwszaPozycjaWPrzestrzeni.z;
                dl1 = Math.hypot(ObecnaPozycja.x, ObecnaPozycja.z);
                dl2 = Math.hypot(pierwszaPozycjaWPrzestrzeni.x, pierwszaPozycjaWPrzestrzeni.z);
            }
             
            // arcsin jest mnożony przez współczynnik by zwiększyć zakres otrzymanego kąta
            kacik = Math.asin(licznik/(dl1*dl2))*4;
           
            // Obrót warstwy o aktualny kąt
            rubikcube.cubeRotation.obrocWarstwe(ObracanaWarstwa.obrot, kacik, false); 
        }
    }
    
    
    public void mouseReleased(MouseEvent e){
        
        if(WarstwaWybrana & Math.abs(kacik) > Math.PI/4)
        {
            // Jeśli kąt jest większy niż 45* uznajemy, że warstwa została obrócona
            double liczbaObrotow = Math.round(Math.abs(kacik)/(Math.PI/2));
            
            // Wyliczenie uzyskanej liczby obrotów o 90*
            liczbaObrotow = liczbaObrotow % 4;
            if(liczbaObrotow == 0){
                rubikcube.cubeRotation.obrocWarstwe(ObracanaWarstwa.obrot, 0, false);
                return;
            }
            
            String akcja;
            akcja = ObracanaWarstwa.obrot;
            
            // Sprawdzamy czy dany obrót jest dodatni czy ujemy
            // Jeśli znak kąta jest różny od kierunku danej obrotu warstwy,
            // zmieniamy akcję (obrót) na przeciwny
            if(ObracanaWarstwa.kierunek*kacik < 0) 
                akcja += "'";
           
            // Obrót warstwy o pełne 90*. 
            for (int i = 0; i < liczbaObrotow; i++) {
                rubikcube.cubeRotation.obrocWarstwe(akcja, Math.PI/2, true); 
                rubikcube.abstractCube.Rotation(akcja);
            }
        }
        else
            // Jeśli warstwa nie została obrócona o więcej niż 45*, uznajemy ruch za nieważny
            rubikcube.cubeRotation.obrocWarstwe(ObracanaWarstwa.obrot, 0, false);
        
        // Reset flagi i przywrócenie możliwości obrotu widoku
        WarstwaWybrana = false;
        OrbitBehavior orbit = new OrbitBehavior(rubikcube.canvas3D, OrbitBehavior.REVERSE_ROTATE);
        orbit.setSchedulingBounds(new BoundingSphere());
        rubikcube.simpleU.getViewingPlatform().setViewPlatformBehavior(orbit);
    }
    
}
